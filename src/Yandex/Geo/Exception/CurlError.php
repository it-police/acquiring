<?php
namespace ITPolice\Acquiring\Yandex\Geo\Exception;

/**
 * Class CurlError
 * @package ITPolice\Acquiring\Yandex\Geo\Exception
 * @license The MIT License (MIT)
 */
class CurlError extends \ITPolice\Acquiring\Yandex\Geo\Exception
{
}