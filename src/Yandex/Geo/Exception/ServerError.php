<?php
namespace ITPolice\Acquiring\Yandex\Geo\Exception;

/**
 * Class ServerError
 * @package ITPolice\Acquiring\Yandex\Geo\Exception
 * @license The MIT License (MIT)
 */
class ServerError extends \ITPolice\Acquiring\Yandex\Geo\Exception
{
}