<?php
namespace ITPolice\Acquiring\Yandex\Geo;

/**
 * Class Exception
 * @package ITPolice\Acquiring\Yandex\Geo
 * @license The MIT License (MIT)
 */
class Exception extends \Exception
{
}