<?php

namespace ITPolice\Acquiring\Yandex\Kassa;

class Settings {

    public $SHOP_PASSWORD;
    public $SECURITY_TYPE;
    public $LOG_FILE;
    public $SHOP_ID;
    public $SCID;
    public $CURRENCY = 10643;
    public $request_source;
    public $mws_cert;
    public $mws_private_key;
    public $mws_cert_password;
    public $demoMode;
    public $formUrl;

    function __construct($SECURITY_TYPE = "MD5" /* MD5 | PKCS7 */, $request_source = "php://input") {
        $this->SHOP_PASSWORD = getenv("YANDEX_SHOP_PASSWORD");
        $this->SHOP_ID = getenv("YANDEX_SHOP_ID");
        $this->mws_cert_password = getenv("YANDEX_CERT_PASSWORD");
        $this->demoMode = getenv("YANDEX_DEMO_MODE");
        $this->SECURITY_TYPE = $SECURITY_TYPE;
        $this->request_source = $request_source;
        $this->LOG_FILE = dirname(__FILE__)."/log.txt";
        $this->mws_cert = dirname(__FILE__)."/mws/shop.cer";
        $this->mws_private_key = dirname(__FILE__)."/mws/private.key";
        if($this->demoMode) {
            $this->SCID = getenv("YANDEX_DEMO_SCID");
            $this->formUrl = 'https://demomoney.yandex.ru/eshop.xml';
        } else {
            $this->SCID = getenv("YANDEX_SCID");
            $this->formUrl = 'https://money.yandex.ru/eshop.xml';
        }
    }
}
