<?php

namespace ITPolice\Acquiring\Yandex\Kassa;

class Form {

    private $settings;

    public function __construct(Settings $settings) {
        $this->settings = $settings;
    }

    public function showForm($sum, $customerNumber, $orderNumber, $fields = array()) {
        if(!$sum || $sum < 0)
            throw new \Exception("Invalid sum value");

        if(!$customerNumber || $customerNumber < 0)
            throw new \Exception("Invalid customerNumber value");

        if(!$orderNumber || $orderNumber < 0)
            throw new \Exception("Invalid orderNumber value");

        $fields = array_replace(
            $fields,
            array(
                'shopId' => $this->settings->SHOP_ID,
                'scid' => $this->settings->SCID,
                'sum' => number_format($sum, 2, '.',''),
                'customerNumber' => $customerNumber,
                'orderNumber' => $orderNumber
            )
        );

        $url =  $this->settings->formUrl;
        ?>

        <form action="<?=$url?>" method="post">

            <?foreach ($fields as $name => $value):?>
                <input name="<?=$name?>" value="<?=$value?>" type="hidden"/>
            <?endforeach;?>

            <input type="submit" value="Перейти на страницу оплаты"/>
        </form>
        <?
    }
}