<?

namespace ITPolice\Acquiring\Sberbank;

class Acquiring
{
    private $userName;
    private $password;
    private $token;
    private $requestUrl;
    private $returnUrl;
    private $failUrl;
    public $lastError = '';
    public $lastUrl = '';
    public $lastParams = '';

    public function __construct()
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }


        if(getenv("SBERBANK_TEST_MODE")) {
            $this->requestUrl = "https://3dsec.sberbank.ru";
            $this->userName = getenv("SBERBANK_TEST_USERNAME");
            $this->password = getenv("SBERBANK_TEST_PASSWORD");
            $this->token = getenv("SBERBANK_TEST_TOKEN");
        } else {
            $this->requestUrl = "https://securepayments.sberbank.ru";
            $this->userName = getenv("SBERBANK_USERNAME");
            $this->password = getenv("SBERBANK_PASSWORD");
            $this->token = getenv("SBERBANK_TOKEN");
        }
        $this->returnUrl = "$protocol://{$_SERVER['HTTP_HOST']}".getenv("SBERBANK_RETURN_URL");
        $this->failUrl = "$protocol://{$_SERVER['HTTP_HOST']}".getenv("SBERBANK_FAIL_URL");

    }


    public function getOrderStatus($orderId)
    {
        $params = [
            'orderId' => $orderId,
            'language' => 'ru',
            'password' => $this->password,
            'userName' => $this->userName,
        ];

        $url = $this->requestUrl."/payment/rest/getOrderStatus.do?".http_build_query($params);

        $this->lastUrl = $url;
        $this->lastParams = $params;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response) {

            $response = json_decode($response, true);

            if ($response['ErrorMessage']) {

                if ($response['OrderStatus'] == 2) {
                    return "Спасибо, заказ №" . $response['OrderNumber'] . " оплачен.";
                } else {
                    return "Статус заказа: " . $response['ErrorMessage'];
                }

            }

        }
        return false;
    }

    public function registerOrder($total, $order)
    {
        $params = [
            'amount' => intval($total).'00',
            'currency' => 643,
            'language' => 'ru',
            'orderNumber' => $order,
            'password' => $this->password,
            'userName' => $this->userName,
            'returnUrl' => $this->returnUrl,
            'failUrl' => $this->failUrl,
        ];
        $url = $this->requestUrl."/payment/rest/register.do?".http_build_query($params);
        $this->lastUrl = $url;
        $this->lastParams = $params;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response) {

            $response = json_decode($response, true);

            if ($response) {
                return $response;
            }

        }
        $this->lastError = $response['errorMessage'];
        return false;
    }

    public function checkSum($params) {
        $checkSum = $params['checksum'];
        $hmac = $this->getHmac($params);
        return $checkSum == $hmac;
    }

    public function getHmac($params) {
        $hashStr = $this->getHashSrt($params);
        $hmac = mb_strtoupper(hash_hmac ( 'sha256' , $hashStr , $this->token));
        return $hmac;
    }

    public function getHashSrt($params) {
        $checkSum = $params['checksum'];
        unset($params['checksum']);
        ksort($params);

        $data = '';
        foreach ($params as $k => $v) {
            $data .="$k;$v;";
        }
        return $data;
    }
}